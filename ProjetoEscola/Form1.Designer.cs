﻿namespace ProjetoEscola
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.Hamburguer1 = new System.Windows.Forms.PictureBox();
            this.Banana2 = new System.Windows.Forms.PictureBox();
            this.Banana1 = new System.Windows.Forms.PictureBox();
            this.Hamburguer2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Hamburguer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Banana2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Banana1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hamburguer2)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Banana",
            "Hamburguer"});
            this.comboBox1.Location = new System.Drawing.Point(99, 54);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.TabStop = false;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Banana",
            "Hamburguer"});
            this.comboBox3.Location = new System.Drawing.Point(586, 54);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 21);
            this.comboBox3.TabIndex = 2;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // Hamburguer1
            // 
            this.Hamburguer1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Hamburguer1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Hamburguer1.BackgroundImage")));
            this.Hamburguer1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Hamburguer1.Location = new System.Drawing.Point(70, 100);
            this.Hamburguer1.Name = "Hamburguer1";
            this.Hamburguer1.Size = new System.Drawing.Size(179, 169);
            this.Hamburguer1.TabIndex = 3;
            this.Hamburguer1.TabStop = false;
            this.Hamburguer1.Visible = false;
            // 
            // Banana2
            // 
            this.Banana2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Banana2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Banana2.BackgroundImage")));
            this.Banana2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Banana2.Location = new System.Drawing.Point(557, 100);
            this.Banana2.Name = "Banana2";
            this.Banana2.Size = new System.Drawing.Size(183, 169);
            this.Banana2.TabIndex = 4;
            this.Banana2.TabStop = false;
            this.Banana2.Visible = false;
            // 
            // Banana1
            // 
            this.Banana1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Banana1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Banana1.BackgroundImage")));
            this.Banana1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Banana1.Location = new System.Drawing.Point(70, 100);
            this.Banana1.Name = "Banana1";
            this.Banana1.Size = new System.Drawing.Size(183, 169);
            this.Banana1.TabIndex = 5;
            this.Banana1.TabStop = false;
            this.Banana1.Visible = false;
            // 
            // Hamburguer2
            // 
            this.Hamburguer2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Hamburguer2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Hamburguer2.BackgroundImage")));
            this.Hamburguer2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Hamburguer2.Location = new System.Drawing.Point(557, 100);
            this.Hamburguer2.Name = "Hamburguer2";
            this.Hamburguer2.Size = new System.Drawing.Size(183, 169);
            this.Hamburguer2.TabIndex = 6;
            this.Hamburguer2.TabStop = false;
            this.Hamburguer2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 285);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Dados sobre o alimento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 311);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Dados sobre o alimento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(86, 340);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Dados sobre o alimento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(595, 285);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Dados sobre o alimento";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(595, 311);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Dados sobre o alimento";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(595, 340);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Dados sobre o alimento";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Hamburguer2);
            this.Controls.Add(this.Banana1);
            this.Controls.Add(this.Banana2);
            this.Controls.Add(this.Hamburguer1);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.comboBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Hamburguer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Banana2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Banana1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hamburguer2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.PictureBox Hamburguer1;
        private System.Windows.Forms.PictureBox Banana2;
        private System.Windows.Forms.PictureBox Banana1;
        private System.Windows.Forms.PictureBox Hamburguer2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

