﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoEscola
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Hamburguer")
            {
                Hamburguer1.Visible = true;
                Banana1.Visible = false;
                label1.Text = "muito gorduros";
                label2.Text = " vai engordar";
                label3.Text = "vai morrer";
            }
            if (comboBox1.Text == "Banana")
            {
                Hamburguer1.Visible = false;
                Banana1.Visible = true;
                label1.Text = "muito saudavel";
                label2.Text = "evita caimbra";
                label3.Text = "bom para a saude";
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.Text == "Banana")
            {
                Banana2.Visible = true;
                Hamburguer2.Visible = false;
                label4.Text = "muito saudavel";
                label5.Text = "evita caimbra";
                label6.Text = "bom para a saude";
            }
            if (comboBox3.Text == "Hamburguer")
            {
                Banana2.Visible = false;
                Hamburguer2.Visible = true;
                label4.Text = "muito gorduros";
                label5.Text = " vai engordar";
                label6.Text = "vai morrer";
            }
        }
    }
}
